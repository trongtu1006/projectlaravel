<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'fullName' => 'required',
            'email' => 'required|string|email',
            'password' => 'required|string|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'phone' => 'required|min:10|numeric',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => ':attribute không được để trống',
            'email' => ':attribute phải đúng định dạng abc..@gmail.com',
            'string' => ':attribute phải là kiểu chuỗi ',
            'min' => ':attribute có ít nhất 10 số',
            'numeric' => ':attribute phải là kiểu số',
            'regex' => ':attribute phải chứa az hoặc AZ và ký tự đặc biệt',
        ];
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        return [
            'fullName' => 'Họ tên',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'phone' => 'Số điện thoại',
        ];
    }
}
