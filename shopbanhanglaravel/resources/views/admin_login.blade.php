<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login admin</title>
    <link rel="icon" href="{{asset('public/frontend/images/logo-mail.png')}}" type="image/gif" sizes="32x32">
</head>
<body>
<div class="container" onclick="onclick">
    <div class="top"></div>
    <div class="bottom"></div>
    <div class="center">
        <form class="center" action="{{URL::to('/admin-dashboard')}}" method="post">
            {{ csrf_field() }}
            <h2>Đăng nhập admin</h2>
            <input type="email" placeholder="E-mail" name="email" value="{{ old('email') }}"/>
            <input type="password" placeholder="Mật khẩu" name="password" value="{{ old('password') }}"/>
            <div class="error-input">
                @if(session()->has('error-login-admin'))
                    {!! session()->get('error-login-admin') !!}
                @endif
            </div>
            <button class="login_admin" style="cursor: pointer; margin: 20px 0 10px 0;">Đăng nhập</button>
        </form>
    </div>
</div>
</body>
</html>
<style>
    @font-face {
        font-family: 'Raleway';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/raleway/v28/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCIPrQ.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Raleway';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/raleway/v28/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCIPrQ.ttf) format('truetype');
    }
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }
    body {
        min-height: 100vh;
        font-family: 'Raleway', sans-serif;
    }
    .container {
        position: absolute;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }
    .container:hover .top:before,
    .container:active .top:before,
    .container:hover .bottom:before,
    .container:active .bottom:before,
    .container:hover .top:after,
    .container:active .top:after,
    .container:hover .bottom:after,
    .container:active .bottom:after {
        margin-left: 200px;
        transform-origin: -200px 50%;
        transition-delay: 0s;
    }
    .container:hover .center,
    .container:active .center {
        opacity: 1;
        transition-delay: 0.2s;
    }
    .top:before,
    .bottom:before,
    .top:after,
    .bottom:after {
        content: '';
        display: block;
        position: absolute;
        width: 200vmax;
        height: 200vmax;
        top: 50%;
        left: 50%;
        margin-top: -100vmax;
        transform-origin: 0 50%;
        transition: all 0.5s cubic-bezier(0.445, 0.05, 0, 1);
        z-index: 10;
        opacity: 0.65;
        transition-delay: 0.2s;
    }
    .top:before {
        transform: rotate(45deg);
        background: #e46569;
    }
    .top:after {
        transform: rotate(135deg);
        background: #ecaf81;
    }
    .bottom:before {
        transform: rotate(-45deg);
        background: #60b8d4;
    }
    .bottom:after {
        transform: rotate(-135deg);
        background: #3745b5;
    }
    .center {
        position: absolute;
        width: 400px;
        height: 400px;
        top: 50%;
        left: 50%;
        margin-left: -200px;
        margin-top: -200px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: 30px;
        opacity: 0;
        transition: all 0.5s cubic-bezier(0.445, 0.05, 0, 1);
        transition-delay: 0s;
        color: #333;
    }
    .center input {
        width: 100%;
        padding: 15px;
        margin: 5px;
        border-radius: 20px;
        border: 1px solid #ccc;
        font-family: inherit;
    }

    .login_admin {
        padding: 12px 45px;
        border-radius: 20px;
        border: 1px solid #FF4B2B;
        background-color: #FF4B2B;
        color: #FFFFFF;
        font-size: 12px;
        font-weight: bold;
        letter-spacing: 1px;
        text-transform: uppercase;
        transition: transform 80ms ease-in;
    }
    .error-input {
        margin-top: 10px;
        color: #f15353;
        height: 11px;
        font-size: 11px;
    }
</style>
