@extends('layout')
@section('content')

    <section id="form"><!--form-->
        <div class="container">
            <div class="row">
                <h2 class="title text-center">Thông tin tài khoản</h2>
                <form action="{{url('/save_identity_user')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-2"></div>
                    <div class="col-sm-5">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {!! session()->get('message') !!}
                            </div>
                        @elseif(session()->has('error'))
                            <div class="alert alert-danger">
                                {!! session()->get('error') !!}
                            </div>
                        @endif
                        <div class="identity-form"><!--login form-->
                            <div class="form-group">
                                <label for="customer_email">Email</label>
                                <input type="email" name="customer_email" class="form-control" id="customer_email" placeholder=".@gmail.com" value="{{ $user->customer_email }}">
                                <small id="emailHelp" class="form-text text-muted">Email có thể thay đổi</small>
                            </div>
                            <div class="form-group">
                                <label for="customer_name">Họ và tên</label>
                                <input type="text" name="customer_name" class="form-control" id="customer_name" placeholder="Nguyễn Văn A" value="{{ $user->customer_name }}">
                                <small id="nameHelp" class="form-text text-muted">Họ và tên có thể thay đổi.</small>
                            </div>
                            <div class="form-group">
                                <label for="customer_phone">Số điện thoại</label>
                                <input type="text" name="customer_phone" class="form-control" id="customer_phone" placeholder="09xxxxxxxx" value="{{ $user->customer_phone }}">
                                <small id="phoneHelp" class="form-text text-muted">Số điện thoại có thể thay đổi</small>
                            </div>
                            <button type="submit" class="btn btn-save">Lưu</button>
                        </div><!--/login form-->
                    </div>
                    <div class="col-md-3">
                        <div class="avatar text-center">
                            <img id="output" src="{{ $user->customer_picture }}">
                            <div class="change-image text-center">
                                <a style="cursor:pointer" id="btn-image" class="btn-image">Chọn ảnh</a>
                                <input name="customer_picture" id="input-image" type="file" accept="image/*" onchange="loadFile(event)" style="display: none;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </form>
            </div>
        </div>
    </section><!--/form-->
    <style>
        .btn:hover {
            color: #FFFFFF;
            background: #f35757;
        }
        .btn-save {
            background: #FE980F;
            border: medium none;
            border-radius: 20px;
            color: #FFFFFF;
            padding: 6px 40px;
        }
        .avatar {
            margin-top: 70px;
        }
        .avatar > img{
            background-position: 50%;
            background-size: cover;
            background-repeat: no-repeat;
            width: 118px;
            height: 118px;
            padding: 10px;
            border-radius: 80px;
            border: 1px solid;
        }
        .change-image {
            margin-top: 15px;
        }
        .btn-image {
            background: #FE980F;
            border: medium none;
            border-radius: 20px;
            color: #FFFFFF;
            padding: 6px 25px;
        }
        .btn-image:hover {
            color: #FFFFFF;
            background: #f35757;
        }
    </style>
    <script type="text/javascript">
        document.getElementById('btn-image').addEventListener('click', () => {
            document.getElementById('input-image').click();
        })

        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endsection
