@extends('layout')

@section('sidebar')
    @include('pages.include.sidebar')
@endsection

@section('content')
    @include('pages.include.slider')
    <div class="col-md-9">
        <div class="features_items"><!--features_items-->

            <div class="category-tab"><!--category-tab-->
                <div class="col-md-12" style="margin-top: 30px;">
{{--                    <h3 class="categor">Danh mục</h3>--}}
                    <div class="owl-carousel owl-theme a">
                        @foreach($cate_pro_tabs as $key => $cat_tab)
                            <div class="item-category">
                                <div class="image-category d-block">
                                    <img src="https://cf.shopee.vn/file/687f3967b7c2fe6a134a2c11894eea4b_tn" alt="">
                                </div>
                                <div class="name-category text-center">
                                    <a href="{{ URL::to('danh-muc/' . $cat_tab->slug_category_product) }}">{{$cat_tab->category_name}}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="tabs_product">

                </div>
            </div>
            <h2 class="title text-center">Sản phẩm mới nhất</h2>
            <div id="all_product"></div>

            <div id="cart_session"></div>
        </div><!--features_items-->
        <style type="text/css">
            div#quick-cart {
                margin-top: 60px;
            }
        </style>

        <!-- Modal giỏ hàng -->
        <div class="modal fade" id="quick-cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Giỏ hàng của bạn</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="show_quick_cart_alert"></div>
                        <div id="show_quick_cart"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal so sanh -->

        <div class="modal fade" id="sosanh" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">So sánh sản phẩm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped" id="row_compare">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Image</th>
                                <th scope="col">View</th>
                                <th scope="col">Management</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal xem nhanh-->
        <div class="modal fade" style="margin-top:50px" id="xemnhanh" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title product_quickview_title" id="">

                            <span id="product_quickview_title"></span>

                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <style type="text/css">
                            span#product_quickview_content img {
                                width: 100%;
                            }
                            @media screen and (min-width: 768px) {
                                .modal-dialog {
                                    width: 700px; /* New width for default modal */
                                }
                                .modal-sm {
                                    width: 350px; /* New width for small modal */
                                }
                            }
                            @media screen and (min-width: 992px) {
                                .modal-lg {
                                    width: 1200px; /* New width for large modal */
                                }
                            }
                        </style>
                        <div class="row">
                            <div class="col-md-5">
                                <span id="product_quickview_image"></span>
                                <span id="product_quickview_gallery"></span>
                            </div>
                            <form>
                                @csrf
                                <div id="product_quickview_value"></div>
                                <div class="col-md-7">
                                    <h2><span id="product_quickview_title"></span></h2>
                                    <p>Mã ID: <span id="product_quickview_id"></span></p>
                                    <p style="font-size: 20px; color: brown;font-weight: bold;">Giá sản phẩm : <span
                                            id="product_quickview_price"></span></p>
                                    <label>Số lượng:</label>
                                    <input name="qty" type="number" min="1" class="cart_product_qty_" value="1"/>
                                    </span>
                                    <h4 style="font-size: 20px; color: brown;font-weight: bold;">Mô tả sản phẩm</h4>
                                    <hr>
                                    <p><span id="product_quickview_desc"></span></p>
                                    <p><span id="product_quickview_content"></span></p>

                                    <div id="product_quickview_button"></div>
                                    <div id="beforesend_quickview"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-default redirect-cart">Đi tới giỏ hàng</button>
                    </div>
                </div>
            </div>
        </div>
        {{--  <ul class="pagination pagination-sm m-t-none m-b-none">
          {!!$all_product->links()!!}
         </ul> --}}
        <!--/recommended_items-->
    </div>
@endsection
<script type="text/javascript">
    $('.a').owlCarousel({
        loop: true,
        margin: 5,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 2
            }
        }
    })
</script>
