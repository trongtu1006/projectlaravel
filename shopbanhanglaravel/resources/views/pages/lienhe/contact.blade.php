@extends('layout')
@section('content')
    <div class="col-md-12">
        <div class="features_items" style="max-height: 600px;"><!--features_items-->
            <h2 class="title text-center">Liên hệ với chúng tôi</h2>
            <div class="row">
                @foreach($contact as $key => $cont)
                    <div class="col-md-4">
                        {!!$cont->info_contact!!}
                        {!!$cont->info_fanpage!!}
                    </div>
                    <div class="col-md-8">
                        <h4 class="map">Bản đồ</h4>
                        {!!$cont->info_map!!}
                    </div>
            </div>
            @endforeach
        </div><!--features_items-->
    </div>
@endsection
